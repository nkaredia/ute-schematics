"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
/**
* @license
* Copyright Google Inc. All Rights Reserved.
*
* Use of this source code is governed by an MIT-style license that can be
* found in the LICENSE file at https://angular.io/license
*/
const core_1 = require("@angular-devkit/core");
const schematics_1 = require("@angular-devkit/schematics");
const ts = require("typescript");
const stringUtils = require("../strings");
const ast_utils_1 = require("../utility/ast-utils");
const change_1 = require("../utility/change");
const find_module_1 = require("../utility/find-module");
const fs = require('fs');

function createi18nFiles(sourceDir, moduleName) {
  const content = "{\n  \n}";
  const encoding = "utf8";
  fs.mkdirSync(sourceDir + '/assets/i18n/' + moduleName);
  fs.writeFileSync(sourceDir + '/assets/i18n/' + moduleName + '/en.json', content);
  fs.writeFileSync(sourceDir + '/assets/i18n/' + moduleName + '/fr.json', content);
}

function addDeclarationToNgModule(options) {
  return (host) => {
    if (!options.module) {
      return host;
    }
    const modulePath = core_1.normalize('/' + options.module);
    const text = host.read(modulePath);
    if (text === null) {
      throw new schematics_1.SchematicsException(`File ${modulePath} does not exist.`);
    }
    const sourceText = text.toString('utf-8');
    const source = ts.createSourceFile(modulePath, sourceText, ts.ScriptTarget.Latest, true);
    const importModulePath = core_1.normalize(`/${options.sourceDir}/${options.path}/` + (options.flat
      ? ''
      : stringUtils.dasherize(options.name) + '/') + stringUtils.dasherize(options.name) + '.module');
    const relativeDir = core_1.relative(core_1.dirname(modulePath), core_1.dirname(importModulePath));
    const relativePath = (relativeDir.startsWith('.')
      ? relativeDir
      : './' + relativeDir) + '/' + core_1.basename(importModulePath);
    const changes = ast_utils_1.addImportToModule(source, modulePath, stringUtils.classify(`${options.name}Module`), relativePath);
    const recorder = host.beginUpdate(modulePath);
    for (const change of changes) {
      if (change instanceof change_1.InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    }
    host.commitUpdate(recorder);
    return host;
  };
}
function default_1(options) {
  options.path = options.path
    ? core_1.normalize(options.path)
    : options.path;
  const sourceDir = options.sourceDir;
  if (!sourceDir) {
    throw new schematics_1.SchematicsException(`sourceDir option is required.`);
  }
  return (host, context) => {
    if (options.module) {
      options.module = find_module_1.findModuleFromOptions(host, options);
    }
    const templateSource = schematics_1.apply(schematics_1.url('./files'), [
      options.spec
        ? schematics_1.noop()
        : schematics_1.filter(path => !path.endsWith('.spec.ts')),
      options.routing
        ? schematics_1.noop()
        : schematics_1.filter(path => !path.endsWith('-routing.module.ts')),
      schematics_1.template(Object.assign({}, stringUtils, {
        'if-flat': (s) => options.flat
          ? ''
          : s
      }, options)),
      schematics_1.move(sourceDir)
    ]);
    try {
      let ret = schematics_1.chain([schematics_1.branchAndMerge(schematics_1.chain([
          addDeclarationToNgModule(options), schematics_1.mergeWith(templateSource)
        ]))])(host, context);
        createi18nFiles(sourceDir, stringUtils.dasherize(options.name));
        return ret;
    } catch (e) {
      console.log(e);
    }
  };
}
exports.default = default_1;
// #
// sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5
// k
// ZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2hhbnNsL1NvdXJjZXMvaGFuc2wvZGV2a2l0LyIsI
// n
// NvdXJjZXMiOlsicGFja2FnZXMvc2NoZW1hdGljcy9hbmd1bGFyL21vZHVsZS9pbmRleC50cyJdLCJ
// u
// YW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBOzs7Ozs7RUFNRTtBQUNGLCtDQUE4RTtBQUM5RSwyR
// E
// Fjb0M7QUFDcEMsaUNBQWlDO0FBQ2pDLDBDQUEwQztBQUMxQyxvREFBeUQ7QUFDekQsOENBQWlEO0F
// B
// Q2pELHdEQUErRDtBQUkvRCxrQ0FBa0MsT0FBc0I7SUFDdEQsTUFBTSxDQUFDLENBQUMsSUFBVSxFQ
// U
// FFLEVBQUU7UUFDcEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM
// s
// Q0FBQztZQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2QsQ0FBQztRQUVELE1BQU0sVUFBVSxHQ
// U
// FHLGdCQUFTLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVuRCxNQUF
// N
// LElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ25DLEVBQUUsQ
// 0
// FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLE1BQU0sSUFBSSxnQ0F
// B
// bUIsQ0FBQyxRQUFRLFVBQVUsa0JBQWtCLENBQUMsQ0FBQztRQUN0RSxDQUFDO1FBQ0QsTUFBTSxVQ
// U
// FVLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQyxNQUFNLE1BQU0
// s
// R0FBRyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxFQUFFLENBQUMsW
// U
// FBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV6RixNQUFNLGdCQUFnQixHQUFHLGd
// C
// QUFTLENBQ2hDLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsSUFBSSxHQUFHO2NBQ
// 3
// RDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyx
// X
// QUFXLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUM7Y0FDL
// 0
// QsV0FBVyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO2NBQ25DLFNBQVMsQ0FDWix
// D
// QUFDO1FBQ0YsTUFBTSxXQUFXLEdBQUcsZUFBUSxDQUFDLGNBQU8sQ0FBQyxVQUFVLENBQUMsRUFBR
// S
// xjQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQzdFLE1BQU0sWUFBWSxHQUFHLENBQUM
// s
// V0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQ
// U
// MsQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO2NBQ2pGLEdBQUcsR0FBRyxlQUFRLENBQUMsZ0JBQWd
// C
// LENBQUMsQ0FBQztRQUNyQyxNQUFNLE9BQU8sR0FBRyw2QkFBaUIsQ0FBQyxNQUFNLEVBQUUsVUFBV
// S
// xFQUNsQixXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksUUFBUSxDQUFDLEV
// B
// QzdDLFlBQVksQ0FBQyxDQUFDO1FBRWhELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQ
// U
// MsVUFBVSxDQUFDLENBQUM7UUFDOUMsR0FBRyxDQUFDLENBQUMsTUFBTSxNQUFNLElBQUksT0FBTyx
// D
// QUFDLENBQUMsQ0FBQztZQUM3QixFQUFFLENBQUMsQ0FBQyxNQUFNLFlBQVkscUJBQVksQ0FBQyxDQ
// U
// FDLENBQUM7Z0JBQ25DLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxNQUF
// N
// LENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEQsQ0FBQztRQUNILENBQUM7UUFDRCxJQUFJLENBQUMsW
// U
// FBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTVCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDZCxDQUF
// D
// LENBQUM7QUFDSixDQUFDO0FBRUQsbUJBQXlCLE9BQXNCO0lBQzdDLE9BQU8sQ0FBQyxJQUFJLEdBQ
// U
// csT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsZ0JBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSx
// D
// QUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDckUsTUFBTSxTQUFTLEdBQ
// U
// csT0FBTyxDQUFDLFNBQVMsQ0FBQztJQUNwQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyx
// D
// QUFDLENBQUM7UUFDZixNQUFNLElBQUksZ0NBQW1CLENBQUMsK0JBQStCLENBQUMsQ0FBQztJQUNqR
// S
// xDQUFDO0lBRUQsTUFBTSxDQUFDLENBQUMsSUFBVSxFQUFFLE9BQXlCLEVBQUUsRUFBRTtRQUMvQyx
// F
// QUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNuQixPQUFPLENBQ
// U
// MsTUFBTSxHQUFHLG1DQUFxQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN4RCxDQUF
// D
// O1FBRUQsTUFBTSxjQUFjLEdBQUcsa0JBQUssQ0FBQyxnQkFBRyxDQUFDLFNBQVMsQ0FBQyxFQUFFO
// 1
// lBQzNDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLGlCQUFJLEVBQUUsQ0FBQyxDQUFDLEN
// B
// QUMsbUJBQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQ
// y
// xVQUFVLENBQUMsQ0FBQztZQUNsRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxpQkFBSSx
// F
// QUFFLENBQUMsQ0FBQyxDQUFDLG1CQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQ
// U
// ksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUMvRSxxQkFBUSxtQkFDSCxXQUFXLEl
// B
// Q2QsU0FBUyxFQUFFLENBQUMsQ0FBUyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDL
// E
// NBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFDNUMsT0FBTyxFQUNWO1lBQ0YsaUJ
// B
// QUksQ0FBQyxTQUFTLENBQUM7U0FDaEIsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLGtCQUFLLENBQ
// U
// M7WUFDWCwyQkFBYyxDQUFDLGtCQUFLLENBQUM7Z0JBQ25CLHdCQUF3QixDQUFDLE9BQU8sQ0FBQzt
// n
// QkFDakMsc0JBQVMsQ0FBQyxjQUFjLENBQUM7YUFDMUIsQ0FBQyxDQUFDO1NBQ0osQ0FBQyxDQUFDL
// E
// lBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNwQixDQUFDLENBQUM7QUFDSixDQUFDO0FBOUJELDR
// C
// QThCQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuKiBAbGljZW5zZVxuKiBDb3B5cmlnaHQgR29vZ
// 2
// xlIEluYy4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cbipcbiogVXNlIG9mIHRoaXMgc291cmNlIGNvZGU
// g
// aXMgZ292ZXJuZWQgYnkgYW4gTUlULXN0eWxlIGxpY2Vuc2UgdGhhdCBjYW4gYmVcbiogZm91bmQga
// W
// 4gdGhlIExJQ0VOU0UgZmlsZSBhdCBodHRwczovL2FuZ3VsYXIuaW8vbGljZW5zZVxuKi9cbmltcG9
// y
// dCB7IGJhc2VuYW1lLCBkaXJuYW1lLCBub3JtYWxpemUsIHJlbGF0aXZlIH0gZnJvbSAnQGFuZ3VsY
// X
// ItZGV2a2l0L2NvcmUnO1xuaW1wb3J0IHtcbiAgUnVsZSxcbiAgU2NoZW1hdGljQ29udGV4dCxcbiA
// g
// U2NoZW1hdGljc0V4Y2VwdGlvbixcbiAgVHJlZSxcbiAgYXBwbHksXG4gIGJyYW5jaEFuZE1lcmdlL
// F
// xuICBjaGFpbixcbiAgZmlsdGVyLFxuICBtZXJnZVdpdGgsXG4gIG1vdmUsXG4gIG5vb3AsXG4gIHR
// l
// bXBsYXRlLFxuICB1cmwsXG59IGZyb20gJ0Bhbmd1bGFyLWRldmtpdC9zY2hlbWF0aWNzJztcbmltc
// G
// 9ydCAqIGFzIHRzIGZyb20gJ3R5cGVzY3JpcHQnO1xuaW1wb3J0ICogYXMgc3RyaW5nVXRpbHMgZnJ
// v
// bSAnLi4vc3RyaW5ncyc7XG5pbXBvcnQgeyBhZGRJbXBvcnRUb01vZHVsZSB9IGZyb20gJy4uL3V0a
// W
// xpdHkvYXN0LXV0aWxzJztcbmltcG9ydCB7IEluc2VydENoYW5nZSB9IGZyb20gJy4uL3V0aWxpdHk
// v
// Y2hhbmdlJztcbmltcG9ydCB7IGZpbmRNb2R1bGVGcm9tT3B0aW9ucyB9IGZyb20gJy4uL3V0aWxpd
// H
// kvZmluZC1tb2R1bGUnO1xuaW1wb3J0IHsgU2NoZW1hIGFzIE1vZHVsZU9wdGlvbnMgfSBmcm9tICc
// u
// L3NjaGVtYSc7XG5cblxuZnVuY3Rpb24gYWRkRGVjbGFyYXRpb25Ub05nTW9kdWxlKG9wdGlvbnM6I
// E
// 1vZHVsZU9wdGlvbnMpOiBSdWxlIHtcbiAgcmV0dXJuIChob3N0OiBUcmVlKSA9PiB7XG4gICAgaWY
// g
// KCFvcHRpb25zLm1vZHVsZSkge1xuICAgICAgcmV0dXJuIGhvc3Q7XG4gICAgfVxuXG4gICAgY29uc
// 3
// QgbW9kdWxlUGF0aCA9IG5vcm1hbGl6ZSgnLycgKyBvcHRpb25zLm1vZHVsZSk7XG5cbiAgICBjb25
// z
// dCB0ZXh0ID0gaG9zdC5yZWFkKG1vZHVsZVBhdGgpO1xuICAgIGlmICh0ZXh0ID09PSBudWxsKSB7X
// G
// 4gICAgICB0aHJvdyBuZXcgU2NoZW1hdGljc0V4Y2VwdGlvbihgRmlsZSAke21vZHVsZVBhdGh9IGR
// v
// ZXMgbm90IGV4aXN0LmApO1xuICAgIH1cbiAgICBjb25zdCBzb3VyY2VUZXh0ID0gdGV4dC50b1N0c
// m
// luZygndXRmLTgnKTtcbiAgICBjb25zdCBzb3VyY2UgPSB0cy5jcmVhdGVTb3VyY2VGaWxlKG1vZHV
// s
// ZVBhdGgsIHNvdXJjZVRleHQsIHRzLlNjcmlwdFRhcmdldC5MYXRlc3QsIHRydWUpO1xuXG4gICAgY
// 2
// 9uc3QgaW1wb3J0TW9kdWxlUGF0aCA9IG5vcm1hbGl6ZShcbiAgICAgIGAvJHtvcHRpb25zLnNvdXJ
// j
// ZURpcn0vJHtvcHRpb25zLnBhdGh9L2BcbiAgICAgICsgKG9wdGlvbnMuZmxhdCA/ICcnIDogc3Rya
// W
// 5nVXRpbHMuZGFzaGVyaXplKG9wdGlvbnMubmFtZSkgKyAnLycpXG4gICAgICArIHN0cmluZ1V0aWx
// z
// LmRhc2hlcml6ZShvcHRpb25zLm5hbWUpXG4gICAgICArICcubW9kdWxlJyxcbiAgICApO1xuICAgI
// G
// NvbnN0IHJlbGF0aXZlRGlyID0gcmVsYXRpdmUoZGlybmFtZShtb2R1bGVQYXRoKSwgZGlybmFtZSh
// p
// bXBvcnRNb2R1bGVQYXRoKSk7XG4gICAgY29uc3QgcmVsYXRpdmVQYXRoID0gKHJlbGF0aXZlRGlyL
// n
// N0YXJ0c1dpdGgoJy4nKSA/IHJlbGF0aXZlRGlyIDogJy4vJyArIHJlbGF0aXZlRGlyKVxuICAgICA
// g
// KyAnLycgKyBiYXNlbmFtZShpbXBvcnRNb2R1bGVQYXRoKTtcbiAgICBjb25zdCBjaGFuZ2VzID0gY
// W
// RkSW1wb3J0VG9Nb2R1bGUoc291cmNlLCBtb2R1bGVQYXRoLFxuICAgICAgICAgICAgICAgICAgICA
// g
// ICAgICAgICAgICAgICAgICBzdHJpbmdVdGlscy5jbGFzc2lmeShgJHtvcHRpb25zLm5hbWV9TW9kd
// W
// xlYCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlUGF0aCk
// 7
// XG5cbiAgICBjb25zdCByZWNvcmRlciA9IGhvc3QuYmVnaW5VcGRhdGUobW9kdWxlUGF0aCk7XG4gI
// C
// AgZm9yIChjb25zdCBjaGFuZ2Ugb2YgY2hhbmdlcykge1xuICAgICAgaWYgKGNoYW5nZSBpbnN0YW5
// j
// ZW9mIEluc2VydENoYW5nZSkge1xuICAgICAgICByZWNvcmRlci5pbnNlcnRMZWZ0KGNoYW5nZS5wb
// 3
// MsIGNoYW5nZS50b0FkZCk7XG4gICAgICB9XG4gICAgfVxuICAgIGhvc3QuY29tbWl0VXBkYXRlKHJ
// l
// Y29yZGVyKTtcblxuICAgIHJldHVybiBob3N0O1xuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBmd
// W
// 5jdGlvbiAob3B0aW9uczogTW9kdWxlT3B0aW9ucyk6IFJ1bGUge1xuICBvcHRpb25zLnBhdGggPSB
// v
// cHRpb25zLnBhdGggPyBub3JtYWxpemUob3B0aW9ucy5wYXRoKSA6IG9wdGlvbnMucGF0aDtcbiAgY
// 2
// 9uc3Qgc291cmNlRGlyID0gb3B0aW9ucy5zb3VyY2VEaXI7XG4gIGlmICghc291cmNlRGlyKSB7XG4
// g
// ICAgdGhyb3cgbmV3IFNjaGVtYXRpY3NFeGNlcHRpb24oYHNvdXJjZURpciBvcHRpb24gaXMgcmVxd
// W
// lyZWQuYCk7XG4gIH1cblxuICByZXR1cm4gKGhvc3Q6IFRyZWUsIGNvbnRleHQ6IFNjaGVtYXRpY0N
// v
// bnRleHQpID0+IHtcbiAgICBpZiAob3B0aW9ucy5tb2R1bGUpIHtcbiAgICAgIG9wdGlvbnMubW9kd
// W
// xlID0gZmluZE1vZHVsZUZyb21PcHRpb25zKGhvc3QsIG9wdGlvbnMpO1xuICAgIH1cblxuICAgIGN
// v
// bnN0IHRlbXBsYXRlU291cmNlID0gYXBwbHkodXJsKCcuL2ZpbGVzJyksIFtcbiAgICAgIG9wdGlvb
// n
// Muc3BlYyA/IG5vb3AoKSA6IGZpbHRlcihwYXRoID0+ICFwYXRoLmVuZHNXaXRoKCcuc3BlYy50cyc
// p
// KSxcbiAgICAgIG9wdGlvbnMucm91dGluZyA/IG5vb3AoKSA6IGZpbHRlcihwYXRoID0+ICFwYXRoL
// m
// VuZHNXaXRoKCctcm91dGluZy5tb2R1bGUudHMnKSksXG4gICAgICB0ZW1wbGF0ZSh7XG4gICAgICA
// g
// IC4uLnN0cmluZ1V0aWxzLFxuICAgICAgICAnaWYtZmxhdCc6IChzOiBzdHJpbmcpID0+IG9wdGlvb
// n
// MuZmxhdCA/ICcnIDogcyxcbiAgICAgICAgLi4ub3B0aW9ucyxcbiAgICAgIH0pLFxuICAgICAgbW9
// 2
// ZShzb3VyY2VEaXIpLFxuICAgIF0pO1xuXG4gICAgcmV0dXJuIGNoYWluKFtcbiAgICAgIGJyYW5ja
// E
// FuZE1lcmdlKGNoYWluKFtcbiAgICAgICAgYWRkRGVjbGFyYXRpb25Ub05nTW9kdWxlKG9wdGlvbnM
// p
// LFxuICAgICAgICBtZXJnZVdpdGgodGVtcGxhdGVTb3VyY2UpLFxuICAgICAgXSkpLFxuICAgIF0pK
// G hvc3QsIGNvbnRleHQpO1xuICB9O1xufVxuIl19